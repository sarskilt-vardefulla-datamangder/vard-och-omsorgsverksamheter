# Exempel i JSON

Samma entitet som i Appendix A, fast nu som JSON.

```json
{
    "id":  "SE2321000131-E000000007687",
    "source": "2120001439",
    "name": "Alepraktiken",
    "type": 4543,
    "company": "Praktikertjänst AB",
    "latitude":  58.399237,
    "longitude":  15.417352,
    "description":  "Alepraktiken, här hjälper vi dig och tar hand om dig när du är sjuk, har skadat dig eller upplever ohälsa. Vi ser till dig och dina behov i alla livets faser, från barndom till ålderdom.",
    "opens": "07:15",
    "closes": "17:00",
    "street":  "Klockarevägen 14",
    "postalcode":  44930,
    "city":  "Nödinge",
    "email":  "",
    "telephone":"+4630397770",
    "URL":  "https://www.ptj.se/alepraktiken"
}
```
