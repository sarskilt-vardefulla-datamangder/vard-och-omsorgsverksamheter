# Olika format
## CSV-formatet

Det enklaste formatet att stödja är CSV formatet enligt [RFC4180](https://datatracker.ietf.org/doc/html/rfc4180). Se exemplet i Appendix A. Det som krävs är att första raden indikerar vilka kolumner som stöds:

<div class="example">
id,source,name,company,latitude,longitude,description,opens,closes,street,postalcode,city,email,telephone,URL
</div>

Utöver det som sägs i [RFC4180](https://datatracker.ietf.org/doc/html/rfc4180) krävs alltid att informationen är uttryckt med teckenkodning UTF-8. På grund av att CSV har funnits länge och det finns en uppsjö av olika ramverk och program som inte alla följer [RFC4180](https://datatracker.ietf.org/doc/html/rfc4180) så uppmanas man följa devisen:

<div class="note">
"Be conservative in what you do, be liberal in what you accept from others"
</div>

Om värden saknas för en viss kolumn i en viss rad utelämnas värdet så att två komman kommer efter varandra. Att stoppa in nollvärden, t.ex. ett mellanslag, ett minus eller texten "null" är inte tillåtet och kommer att misstolkas.

### CSV-schema

Du som är nöjd med att leverera eller använda data utifrån vad som beskrivs skriftligen i denna specifikation och exemplifieras i appendixen kan ignorera detta avsnitt.

Sedan 2015 finns, "[Metadata Vocabulary for Tabular Data](https://www.w3.org/TR/tabular-metadata/)", en W3C rekommendation för hur man anger metadata för datamodeller uttryckta i CSV.

<div class="def">
**Schema i JSON för Vård- och omsorgsverksamheter:** [schema.json](./schema.json)
</div>

Syfte med schemat är att möjliggöra maskinell verifering av om CSV-uttryck följer denna specifikation.

## JSON formatet

JSON uttrycket följer direkt från CSV-schemat och W3C-rekommendationen "[Generating JSON from Tabular Data on the Web](https://www.w3.org/TR/csv2json/)". Se exempel på JSON i appendix B.
