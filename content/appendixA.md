## Exempel i CSV, kommaseparerad

<div class="example csvtext">
id,source,organization,name,type,company,latitude,longitude,description,openinghours,opens,closes,street,postalcode,city,region,email,telephone,URL,image
<br>

SE2321000131-E000000007687,2120001439,Alepraktiken,3004,"Praktikertjänst AB,58.399237,15.417352,Alepraktiken - här hjälper vi dig och tar hand om dig när du är sjuk har skadat dig eller upplever ohälsa. Vi ser till dig och dina behov i alla livets faser - från barndom till ålderdom.",07:15,17:00,Klockarevägen 14,44930,Nödinge,,+463039770,https://www.ptj.se/alepraktiken/
</div>

