**Bakgrund**

Syftet med denna specifikation är att beskriva information om vård- och omsorgsverksamheter på ett enhetligt och standardiserat vis. 

Specifikationen syftar till att ge kommuner och regioner i Sverige möjlighet att enkelt kunna sätta samman och publicera datamängd(er) som beskriver vårdverksamheter. Den syftar även till att göra det enklare för internationella användare som är tagare av datamängden. 

Följande har deltagit:

**[Dataverkstad](https://www.vgregion.se/ov/dataverkstad/)** - Modellering och rådgivning.<br>



