# Beskrivning i en datakatalog [DCAT-AP-SE 2.0](https://docs.dataportal.se/dcat/sv/)

För att visa att din datamängd följer en specifikation, ska du markera dessa datamängder enligt nedan. 

## Datamängd - uppfyller specifikation

På datamängden peka ut denna specifikation [https://dataportal.se/specifications//vard-och-omsorgsverksamheter/1.0/](https://dataportal.se/specifications/xx/1.0/) via egenskapen (property) `dcterms:conformsTo` med fältnamnet [Uppfyller](https://docs.dataportal.se/dcat/sv/#dcat_Dataset-dcterms_conformsTo). Detta attribut är valfritt och syns inte på obligatorisk respektive rekommenderad. I fältet kopierar ni in den fullständiga länken angiven ovan. 

## Distribution - möter specifikation

På distributionen som motsvarar CSV-filen bör du peka ut [schemat](https://lankadedata.se/spec/vard-och-omsorgsverksamheter/schema.json) via egenskapen `dcterms:conformsTo` med fältnamnet [Länkade scheman](https://docs.dataportal.se/dcat/sv/#dcat_Dataset-dcterms_conformsTo). 

## Minsta krav på datamängdens metadata

I specifikationen [DCAT-AP-SE](https://www.w3.org/TR/vocab-dcat/) anges [vilka fält som måste anges när man beskriver en datamängd](https://docs.dataportal.se/dcat/sv/#dcat%3ADataset). Då denna specifikation skrivs är det titel, beskrivning och utgivare som är obligatoriska. Utöver dessa fält bör följande fält fyllas i:

1. **Uppdateringsfrekvens** eller **ändringsdatum** - om datamängden uppdateras regelbundet eller automatiskt räcker det att man anger en uppdateringsfrekvens. Om det sker oregelbundet är det lämpligt att man sätter ett ändringsdatum. Detta enligt [rekommendation 7 på dataportalens dokumentation](https://docs.dataportal.se/dcat/docs/recommendations/#7-utgivningsdatum-modifieringsdatum-och-uppdateringsfrekvens).
    
2. **Kontaktuppgift** - en kontaktväg för frågor eller synpunkter kring data. Det kan vara en bevakad postlåda eller tjänst för att konsumenter ska komma i kontakt med dataproducenten.

För att ditt data ska möta förväntade behov och vara till nytta för en datakonsument, måste diskussion om datat vara möjligt - felanmälan, frågor eller förbättringsförslag måste kunna tas emot och värderas. Öppna data måste vara “levande” och i dialog med den som konsumerar datat kan du skapa en bättre, mer korrekt och därmed en bättre nyttjad datamängd.

## Licens

Licensen för din datamängd [uttrycks på distributionsnivån](https://docs.dataportal.se/dcat/sv/#dcat_Distribution-dcterms_license).

Licensen för data som släpps enligt denna specifikation bör vara [CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.sv), den helt fria och öppna licensen för Creative Commons. **_Detta är en stark rekommendation och annan licensform bör motiveras och användas endast i sällsynta undantagsfall!_**

[CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.sv) betyder att inga kostnader, begränsningar eller hinder finns för vem som helst att använda, transformera, länka och hantera datat utan krav på motprestation. Läs mer på [Creative Commons](https://creativecommons.org/about/cclicenses/) om de olika licenser som ingår i definitionen.

För att data som denna specifikation definierar ska få maximal spridning, användning och ge störst nytta måste den släppas under en fri licens och inte hämmas av krav.
