# Datamodell

Datamodellen är tabulär, det innebär att varje rad motsvarar exakt en verksamhet och varje kolumn motsvarar en egenskap för den beskrivna verksamheten. 20 attribut är definierade, där de första 8 är obligatoriska. Speciellt viktigt är att man anger de obligatoriska fält i datamodellen som anges. 

<div class="note" title="1">
Vi väljer att använda beskrivande men korta kolumnnamn som uttrycks med gemener, utan mellanslag (understreck för att separera ord) och på engelska. Vård- och omsorgsverksamhet skrivs nedan som vårdverksamhet eller endast verksamhet för att underlätta läsandet av datamodellen.

Genom engelska attributnamn blir modellen enklare att hantera i programvaror och tjänster utvecklade utanför Sverige.


</div>

<div class="ms_datatable">

| Namn  | Kardinalitet      | Datatyp                         | Beskrivning|
 -- | :---------------:| :-------------------------------: | ---------------------- |
|[**id**](#id)|1|[HSAID](#id)|**Obligatoriskt** -  HSAID för vårdverksamheten.|
|[**source**](#source)|1|[heltal](#heltal)|**Obligatoriskt** - Organisationsnumret för utförande organisation.|
|[**organization**](#organization)|1|NonGovernmentalOrganisation &vert; RegionalAuthority &vert; LocalAuthority|**Obligatoriskt** - I vilken regi vårdverksamheten bedrivs.|
|[**name**](#name)|1|text|**Obligatoriskt** - Namnet på vårdverksamheten.|
|[**type**](#type)|1..n|[concept](#concept)| **Obligatoriskt** - Vilka typer av vårdenheter som bedrivs inom verksamheten.|
|[**practitioner**](#practitioner)|1|text|**Obligatoriskt** - Namnet på utförare.|
|[**latitude**](#latitude)|1|[decimal](#decimal)|**Obligatoriskt** - Latitude anges per format enligt WGS84.|
|[**longitude**](#longitude)|1|[decimal](#decimal)|**Obligatoriskt** - Longitude anges per format enligt WGS84.|
|[description](#description)|0..1|text|Beskrivning av verksamheten.|
|openinghours|0..1|text|En ytterligare beskrivning av öppettiderna som exempelvis vanligt förekommande öppettider.|
|[opens](#opens)|0..1|[time](#time)|När verksamheten öppnar.|
|[closes](#closes)|0..1|[time](#time)|När verksamheten stänger.|
|[street](#street)|0..1|text|Gatuadress för verksamheten.|
|[postalcode](#postalcode)|0..1|[heltal](#heltal)|Postnummer till verksamheten.|
|[city](#city)|0..1|text|Postort för verksamheten.|
|[region](#region)|0..1|text|Regionen där verksamheten finns.|
|[email](#email)|0..1|text|E-postadress för vidare kontakt, anges med gemener och med @ som avdelare.|
|[telephone](#telephone)|0..1|[heltal](#heltal)|Telefonnummer till verksamheten.|
|[URL](#url)|0..1|[URL](#url)|Ingångssida för mer information om verksamheten.|
|[image](#image)|0..*|[URL](#url)|Länk till bild på verksamheten.|

</div>

## Förtydligande av datatyper

En del av datatyperna nedan förtydligas med hjälp av det som kallas reguljära uttryck. Dessa är uttryckta så att de matchar exakt, dvs inga inledande eller eftersläpande tecken tillåts.

### **heltal**
Reguljärt uttryck: **`/^\-?\\d+$/`**

Heltal anges alltid som en rad siffror utan mellanrum eventuellt med ett inledande minus. Se [xsd:integer](https://www.w3.org/TR/xmlschema-2/#integer) för en längre definition. Utelämnat värde tolkas aldrig som noll (0) utan tolkas som “avsaknad av värde”.
 
### **decimal**
Reguljärt uttryck: **`/^\-?\\d+\\.\\d+$/`**

Decimaltal anges i enlighet med [xsd:decimal](https://www.w3.org/TR/xmlschema-2/#decimal). Notera att i Sverige används ofta decimalkomma inte punkt. För att vara enhetlig mellan olika dataformat ska decimalpunkt användas då den tabulära modellen använder komma som separator.

Den kanoniska representationen i xsd:decimal är påbjuden, d.v.s. inga inledande nollor eller +, samt att man alltid ska ha en siffra innan och efter decimalpunkt. Noll skrivs som 0.0 och ett utelämnat värde skall aldrig tolkas som noll (0) utan “avsaknad av värde”.

### **url**

Reguljärt uttryck: **`/^(http|https):\/\/[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*\.[a-zA-Z]{2,}(\/[a-zA-Z0-9\-._~:/?#\[\]@!$&'()*+,;%=]*)?$/`**

En länk till en webbsida där verksamheten presenteras hos den lokala myndigheten eller organisation.

Observera att man inte får utelämna schemat, d.v.s. "www.example.com" är inte en tillåten webbadress, däremot är "https://www.example.com" ok. Relativa webbadresser accepteras inte heller. (Ett fullständigt reguljärt uttryck utelämnas då den är både för omfattande och opedagogisk.)

Om du behöver ange flera URL:er måste du då sätta dubbelt citattecken och separera de olika URL:erna med kommatecken. Läs gärna med i RFC 4180 för CSV.

Kontrollera även innan du publicerar filen att det går att läsa in din fil utan problem. Det finns flera webbverktyg för att testa så kallad [parsing](https://sv.wikipedia.org/wiki/Parser) t.ex. [https://CSVLint.io](https://csvlint.io) som du kan använda för att testa din fil.

### **boolean**

Reguljärt uttryck: **`/^[true|false]?&/`**

I samtliga förekommande fall kan texten “true” eller “false” utelämnas.

Ett tomt fält skall tolkas som “okänt” eller “ej inventerat”. Ange enbart “true” eller “false” om du vet att egenskapen finns (true) eller saknas (false). Gissa aldrig.

Attributet är en så kallad Boolesk” datatyp och kan antingen ha ett av två värden: TRUE eller FALSE, men aldrig båda.

### time 

En tidpunkt som återkommer på flera dagar i formen hh:mm:ss[Z|(+|-)hh:mm] (se XML-schema för detaljer)[http://www.w3.org/TR/xmlschema-2/#time].

hh:mm:ss eller hh:mm 

Exempel - en verksamhet som stänger kl 5 på eftermiddagen. **17:00** 

</div>

## Förtydligande av attribut

### **id**

Reguljärt uttryck: **`/^[a-zA-Z_:1-9]*$/`**

Anger identifierare, HSAID, för verksamheten. 

HSAID kommer från [Ineras katalogtjänst HSA](https://www.inera.se/tjanster/alla-tjanster-a-o/hsa-katalogtjanst/) och ger möjligheten att unikt referera till en specifik vårdverksamhet. 

Exempelvis har Alepraktiken HSAID: SE2321000131-E000000007687

### source

Reguljärt uttryck: **`/^[1-9]\d*$/`**

Anger organisationsnummret utan mellanslag eller bindesstreck för den utförande organisationen. Exempel för Gullspångs kommun: **2120001637**.

### organization

Reguljärt uttryck: **`/^(NonGovernmentalOrganisation|RegionalAuthority|LocalAuthority)$/`**

Anger i vilken regi verksamheten bedrivs. Värden som kan anges: NonGovernmentalOrganisation (privat regi), RegionalAuthority (regional regi) och LocalAuthority (kommunal regi). Endast ett värde får anges, inte fler.

### name

Reguljärt uttryck: **`/^[^'"]*$/`**

Anger namnet på verksamheten. Exempelvis: Borås Vårdcentral.

### type 

Anger typen av vårdenheter som finns inom verksamheten. [Verksamhetskoder](https://inera.atlassian.net/wiki/spaces/OIKH/pages/346560593/HSA+kodverk)

<br> 

Flera koder kan anges och separera dessa med mellanslag eller annan separator som inte är ett komma.

### practitioner

Reguljärt uttryck: **`/^".*?"$/`**

Anger namnet på aktören som utför verksamheten. Exempelvis: Achima Care AB. 

### latitude

Reguljärt uttryck: **`/^-?\d+(\.\d+)?$/`** 

WGS84 är den standard som det amerikanska systemet GPS använder för att beskriva en latitud på jordklotets yta. GPS används av många kartapplikationer. Latitud anges med ett heltal följt av en decimalpunkt “.” och 1 till 8 decimaler. Exempelvis “61.21657”. En angivelse av latitud som befinner sig på jordens södra hemisfär anges med negativt tal. Exempelvis “-53.78589”. Om koordinatens inledande heltal är noll, skall alltid nollan anges. 

### longitude

Reguljärt uttryck: **`/^-?\d+(\.\d+)?$/`** 

Longitud anges med ett heltal följt av en decimalpunkt följt av 1 till 8 decimaler. Exempelvis “88.40901”. En longitud som ligger väster om WGS84-systemets meridian, anges med negativt tal, Exempelvis: “-0.158101”. Om koordinatens inledande heltal är noll, skall nollan alltid anges.

### description 

Reguljärt uttryck: **`/^".*?"$/`**

Anger ytterligare information om verksamheten som kompletterar resterande attribut i datamodellen. Exempelvis: Alepraktiken, här hjälper vi dig och tar hand om dig när du är sjuk, har skadat dig eller upplever ohälsa. Vi ser till dig och dina behov i alla livets faser, från barndom till ålderdom.

### opens

Reguljärt uttryck: **`/^([01]?[0-9]|2[0-3]):[0-5][0-9]-([01]?[0-9]|2[0-3]):[0-5][0-9]$/`**

Anger när verksamheten öppnar enligt datatypen [time](#time) som är beskriven ovan. 

### closes 

Reguljärt uttryck: **`/^([01]?[0-9]|2[0-3]):[0-5][0-9]-([01]?[0-9]|2[0-3]):[0-5][0-9]$/`**

Anger när verksamheten stänger enligt datatypen [time](#time) som är beskriven ovan. 

### street

Reguljärt uttryck: **`/^[a-zA-Z0-9 ]*$/`**

Anger verksamhetens gatuadress.

### postalcode

Reguljärt uttryck: **`/\b(0{1,4}[1-9]|[1-9]\d{0,4}|[1-9])\b/`**

Anger postnummer. Ange inte mellanslag. 

### city

Reguljärt uttryck: **`/^[^'"]*$/`**

Anger kommun.

### region

Reguljärt uttryck: **`/^[^'"]*$/`**

Anger region.

### email

Reguljärt uttryck: **`/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/`**

Anger funktionsadress till den organisation som ansvarar för information om verksamheten. Ange ej det inledande URI schemat mailto: eller HTML-koder i uttrycket. Exempel: example@domain.se

### telephone 

Reguljärt uttryck: **`/^\+?\d{1,3}[-.\s]?\(?\d{1,4}\)?[-.\s]?\d{1,4}[-.\s]?\d{1,4}$/`**

Anger telefonnummer till verksamheten med inledande landskod. Exempelvis: +4630397770

### image

Reguljärt uttryck: **`/^(http|https):\/\/[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*\.[a-zA-Z]{2,}(\/[a-zA-Z0-9\-._~:/?#\[\]@!$&'()*+,;%=]*)?$/`**

Anger länk till bild. Ange inledande schemat https:// eller http:// 
 




