{
  "@context": [
    "http://www.w3.org/ns/csvw",
    {
      "@language": "sv",
      "schema": "http://schema.org/",
      "dct": "http://purl.org/dc/terms/",
      "fhir": "http://hl7.se/fhir/ig/base/StructureDefinition/SEBaseOrganization"
    }
  ],
  "dc:title": "CSV schema för Vård- och omsorgsverksamheter",
  "dcat:keyword": [
    "vårdverksamhet"
  ],
  "dc:license": {
    "@id": "http://opendefinition.org/licenses/cc-by/"
  },
  "dc:modified": {
    "@value": "2024-02-20",
    "@type": "xsd:date"
  },
  "tableSchema": {
    "columns": [
      {
        "name": "id",
        "titles": ["Identifierare"],
        "dc:description": "HSAID för vårderksamheten",
        "datatype": "string",
        "required": true,
        "propertyUrl": "dct:identifier"
      },
      {
        "name": "source",
        "titles": "Källa",
        "dc:description": "Organisationsnummer för utförande organisation",
        "datatype": "integer",
        "required": true,
        "propertyUrl": "dct:publisher"
      },
      {
        "name": "organization",
        "titles": "Regi",
        "dc:description": "I vilken regi vårdverksamheten bedrivs",
        "datatype": {
          "base": "string",
          "format": "NonGovernmentalOrganisation|RegionalAuthority|LocalAuthority"
      },
        "required": true,
        "propertyUrl": "dct:type",
        "valueUrl": "http://purl.org/adms/publishertype/{organization}"
      },
      {
        "name": "name",
        "titles": "Namn",
        "dc:description": "Namn på vårdverksamheten",
        "datatype": "string",
        "required": true,
        "propertyUrl": "schema:name"
      },
      {
        "name": "type",
        "titles": "Typ",
        "dc:description": "Typ av vårdenhet",
        "datatype": "integer",
        "required": true,
        "propertyUrl": "rdf:type"
      },
      {
        "name": "practitioner",
        "titles": "Utförare",
        "dc:description": "Namn på utförande aktör",
        "datatype": "string",
        "required": true,
        "propertyUrl": "schema:organization"
      },
      {
        "name": "latitude",
        "titles": "Latitud",
        "dc:description": "Latitud enligt WGS84.",
        "datatype": "decimal",
        "required": true,
        "propertyUrl": "schema:latitude"
      },
      {
        "name": "longitude",
        "titles": "Longitude",
        "dc:description": "Longitud enligt WGS84.",
        "datatype": "decimal",
        "required": true,
        "propertyUrl": "schema:longitude"
      },
      {
        "name": "description",
        "titles": "Beskrivning",
        "dc:description": "Beskrivning av verksamheten.",
        "datatype": "string",
        "propertyUrl": "dct:description"
      },
      {
        "name": "openinghours",
        "titles": "Öppettider",
        "dc:description": "Ytterligare information om öppettider.",
        "datatype": "string",
        "propertyUrl": "schema:openingHours"
      },
      {
        "name": "opens",
        "titles": "Öppnar",
        "dc:description": "När verksamheten öppnar.",
        "datatype": "time",
        "propertyUrl": "schema:opens"
      },
      {
        "name": "closes",
        "titles": "Stänger",
        "dc:description": "När verksamheten stänger.",
        "datatype": "time",
        "propertyUrl": "schema:closes"
      },
      {
        "name": "street",
        "titles": "Gatuadress",
        "dc:description": "Gatuadress för verksamheten.",
        "datatype": "string",
        "propertyUrl": "schema:streetAddress"
      },
      {
        "name": "postalcode",
        "titles": "Postnummer",
        "dc:description": "Postnummer för verksamheten.",
        "datatype": {
          "base": "integer",
          "minimum": 10000,
          "maximum": 99999
        },
        "propertyUrl": "schema:postalCode"
      },
      {
        "name": "city",
        "titles": "Postort",
        "dc:description": "Postort för verksamheten.",
        "datatype": "string",
        "propertyUrl": "schema:addressLocality"
      },
      {
        "name": "email",
        "titles": "Email",
        "dc:description": "Mejladress för vidare kontakt.",
        "datatype": "string",
        "propertyUrl": "schema:email"
      },
      {
        "name": "telephone",
        "titles": "Telefonnummer",
        "dc:description": "Telefonnummer till verksamheten.",
        "datatype": "string",
        "propertyUrl": "schema:telephone"
      },
      {
        "name": "URL",
        "titles": "Ingångssida",
        "dc:description": "Ingångssida för verksamheten.",
        "datatype": "url",
        "propertyUrl": "schema:url"
      },
      {
        "name": "image",
        "titles": "Bild",
        "dc:description": "Länk till bilder för verksamheter.",
        "datatype": "url",
        "propertyUrl": "schema:image"
      }
    ],
    "primaryKey": "id",
    "aboutUrl": "#gid-{id}"
  }
}
